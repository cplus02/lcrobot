*** Settings ***
Documentation               Web element repository

*** Variables ***
## Login Page
${webAgentLoginEmail}       name=email
${webAgentLoginPassword}    name=password
${webAgentLoginButton}      xpath=//*[@id="app"]/div/div/div/div/div/div[2]/form/div[4]/div/button

## Dashboard
${webAgentChatPageUrlTB}    https://mconnect-tb.m800.com/chat

${webAgentOnlineStatus}     xpath=//*[@id="app"]/div/div/div/div/div/main/div[1]/div[3]/div/div/div[2]/span

${webAgentIncomingImName}   xpath=//*[@id="app"]/div/div/div/div/div/main/section/div/div[1]/div/div/div/ul/li/div[1]/span[2][contains(., 'visitor')]
${webAgentIncomingImTag}    xpath=//*[@id="app"]/div/div/div/div/div/main/section/div/div[1]/div/div/div/ul/li/div[2]/div[1]
${webAgentIncomingImTime}   xpath=//*[@id="app"]/div/div/div/div/div/main/section/div/div[1]/div/div/div/ul/li/div[1]/span[3]/span
${webAgentIncomingImLng}    xpath=//*[@id="app"]/div/div/div/div/div/main/section/div/div[1]/div/div/div/ul/li/div[2]/div[2]
${webAgentIncomingImIcon}   xpath=//*[@id="app"]/div/div/div/div/div/main/section/div/div[1]/div/div/div/ul/li/div[1]/span[1]
${webAgentPickupedImName}   xpath=//*[@id="app"]/div/div/div/div/div/main/section/div/div[1]/div/div/div/ul/li/div/div[3]/span[1][contains(., 'visitor')]

## Chatroom
${webAgentChatroomHeader}   xpath=//*[@id="app"]/div/div/div/div/div/main/section/div/div[2]/div/div/div[1]/div[1]/div/div[2]/div[1][contains(., 'visitor')]
${webAgentChatClose}        xpath=/html/body/div[1]/div/div/div/div/div/main/section/div/div[2]/div/div/div[1]/div[1]/div/div[3]/button
${webAgentChatCloseTitle}   Close Inquiry
${webAgentChatCloseYes}     xpath=/html/body/div[2]/div/div/div[3]/button[2]
${webAgentCharCloseNo}      xpath=/html/body/div[2]/div/div/div[3]/button[1]


*** Settings ***
Library                     String
Library                     Selenium2Library
Resource                    webCfg.robot
Resource                    webElement.robot

*** Keywords ***
Login To Web Agent
    Selenium2Library.Open Browser  ${webAgentUrlTB}  ff
    Selenium2Library.Wait Until Page Contains Element  ${webAgentLoginEmail}  timeout=30s
    Selenium2Library.Input Text  ${webAgentLoginEmail}  ${webAgentTestUser}
    Selenium2Library.Input Text  ${webAgentLoginPassword}  ${webAgentTestPassword}
    Selenium2Library.Click Element  ${webAgentLoginButton}
    Selenium2Library.Wait Until Page Contains Element  ${webAgentOnlineStatus}  timeout=30s
    Selenium2Library.Element Text Should Be  ${webAgentOnlineStatus}  Online
    Selenium2Library.Location Should Be  ${webAgentChatPageUrlTB}

Reset Web Agent Test Env
    Sleep  5s
    :For  ${existedInquiry}  IN RANGE  1  99
    \  ${status}  Run Keyword And Return Status  Selenium2Library.Page Should Contain Element  ${webAgentPickupedImName}
    \  Run Keyword If  ${status}  Run Keywords  Selenium2Library.Click Element  ${webAgentPickupedImName}
    ...  AND  Selenium2Library.Click Element  ${webAgentChatClose}
    ...  AND  Selenium2Library.Wait Until Page Contains  ${webAgentChatCloseTitle}  timeout=30s
    ...  AND  Selenium2Library.Click Element  ${webAgentChatCloseYes}
    ...  AND  Sleep  1s
    ...  ELSE  Exit For Loop


Web Agent Pick Up Im Inquiry
    Sleep  2s
    ${status}  Run Keyword And Return Status  Selenium2Library.Page Should Contain Element  ${webAgentPickupedImName}
    Run Keyword If  ${status}  Selenium2Library.Click Element  ${webAgentPickupedImName}
    ...         ELSE  Selenium2Library.Click Element  ${webAgentIncomingImName}
    Selenium2Library.Wait Until Page Contains Element  ${webAgentChatroomHeader}
    Sleep  2s
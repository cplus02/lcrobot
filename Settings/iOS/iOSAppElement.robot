*** Settings ***
Documentation               iOS app element repository

*** Variables ***
## Landing Page
${iosWidgetButton}          xpath=//*[@name="LiveConnect"]/XCUIElementTypeWindow[2]/XCUIElementTypeOther[1]/XCUIElementTypeButton[1]

## Directory Listing Page
${iosDirectoryPageTitle}    name=Directory
${iosDirectoryPageSearch}   name=Search
${iosDirectoryCallButton}   xpath=//XCUIElementTypeApplication[@name="LiveConnect"]/XCUIElementTypeWindow[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell/XCUIElementTypeButton[1]
${iosDirectoryImButton}     xpath=//XCUIElementTypeApplication[@name="LiveConnect"]/XCUIElementTypeWindow[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell/XCUIElementTypeButton[2]

## Chat
${iosChatFindingAgent}      name=Finding agent
${iosChatCallBtn}           name=directory white call
${iosChatFileSharingBtn}    xpath=//XCUIElementTypeApplication[@name="LiveConnect"]/XCUIElementTypeWindow[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther[2]/XCUIElementTypeButton[1]   
${iosChatSendBtn}           name=micButton
${iosChatCloseBtn}          name=cross shape
${iosChatBackBtn}           name=btn back
${iosChatInputField}        xpath=//XCUIElementTypeApplication[@name="LiveConnect"]/XCUIElementTypeWindow[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther[2]/XCUIElementTypeScrollView/XCUIElementTypeTextView

${iosChatCloseTitle}        Close Inquiry
${iosChatCloseKeep}         Keep
${iosChatCloseClose}        Close

${iosChatClosedTitle}       Inquiry is Terminated
${iosChatClosedFind}        Find Another Agent
${iosChatClosedClose}       Leave Chatroom
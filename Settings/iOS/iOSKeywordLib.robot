*** Settings ***
Library                     String
Library                     AppiumLibrary
Resource                    iOSDeviceCfg.robot
Resource                    iOSAppElement.robot

*** Keywords ***
Initialize iOS Device
    [Arguments]  ${iOSDevice}=iOSDevice01
    AppiumLibrary.Open Application  ${appiumUrliOS}  automationName=${automationNameiOS}  platformName=${platformNameiOS}
    ...  bundleId=${bundleIdTestbed}  autoAcceptAlerts=${autoAcceptAlerts}
    ...  platformVersion=&{${iOSDevice}}[platformVersion]  deviceName=&{${iOSDevice}}[deviceName]
    ...  udid=&{${iOSDevice}}[udid]  noReset=&{${iOSDevice}}[noReset]

## Directory
Make An Inquiry By Im On iOS App Client
    AppiumLibrary.Click Element  ${iosWidgetButton}
    Sleep  2s  ## Wait for slower status
    ${status}  Run Keyword And Return Status  AppiumLibrary.Page Should Contain Text  Finding agent
    Run Keyword If  ${status}  Run Keywords  AppiumLibrary.Click Element  ${iosChatBackBtn}
    ...  AND  AppiumLibrary.Wait Until Page Contains Element  ${iosWidgetButton}  timeout=30s
    ...  AND  AppiumLibrary.Click Element  ${iosWidgetButton}
    AppiumLibrary.Wait Until Page Contains Element  ${iosDirectoryPageSearch}  timeout=30s
    AppiumLibrary.Input Text  ${iosDirectoryPageSearch}  TW Dev
    AppiumLibrary.Click Element  ${iosDirectoryImButton}
    ${status}  Run Keyword And Return Status  AppiumLibrary.Page Should Contain Text  ${iosChatClosedTitle}
    Run Keyword If  ${status}  AppiumLibrary.Click Text  ${iosChatClosedFind}

## Chat
Send Message From App Client
    [Arguments]  ${messageString}
    AppiumLibrary.Element Should Be Enabled  ${iosChatSendBtn}
    AppiumLibrary.Input Text  ${iosChatInputField}  ${messageString}
    AppiumLibrary.Click Element  ${iosChatSendBtn}
    AppiumLibrary.Page Should Contain Text  ${messageString}

Close App Client Chatroom
    ${status}  Run Keyword And Return Status  AppiumLibrary.Element Should Be Enabled  ${iosChatCloseBtn}
    Run Keyword If  ${status}  AppiumLibrary.Click Element  ${iosChatCloseBtn}
    AppiumLibrary.Wait Until Page Contains  ${iosChatCloseTitle}  timeout=30s
    AppiumLibrary.Click Text  ${iosChatCloseClose}
    Sleep  2s

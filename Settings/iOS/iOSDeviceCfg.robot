*** Settings ***
Documentation               iOS devices configuration file, including udid, appium server URL...etc

*** Variables ***
## Common system environment variables for iOS device
${appiumUrliOS}             http://localhost:4723/wd/hub
${automationNameiOS}        XCUITest
${platformNameiOS}          iOS
${bundleIdTestbed}          com.m800.liveconnect.testbed  ##For testbed environment
${autoAcceptAlerts}         true  ##Automatically accept alerts such as privacy, location...             

## Add real iOS device info here
&{iOSDevice01}              platformVersion=11.4  deviceName=iPhone  browserName=Safari
...                         udid=d958aee95d9349d30cecec666710a803fcfdb10e  noReset=false
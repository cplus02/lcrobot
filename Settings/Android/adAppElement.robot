*** Settings ***
Documentation               Android app element repository

*** Variables ***
## Landing Page
${adWidgetButton}           id=com.m800.liveconnectdemo:id/lc_widget_button_icon

## Directory Listing Page
${adDirectoryPageTitle}     Directory
${adDirectoryPageSearchBtn}  id=com.m800.liveconnectdemo:id/search_button
${adDirectoryPageSearch}    id=com.m800.liveconnectdemo:id/search_src_text
${adDirectoryCallButton}    id=com.m800.liveconnectdemo:id/lc_btn_call
${adDirectoryImButton}      id=com.m800.liveconnectdemo:id/lc_img_chat_icon

## Chat
${adChatFindingAgent}       Finding agent
${adChatCallBtn}            id=com.m800.liveconnectdemo:id/live_connect_call
${adChatFileSharingBtn}     id=com.m800.liveconnectdemo:id/lc_btn_attach_file
${adChatMicBtn}             id=com.m800.liveconnectdemo:id/lc_btn_mic
${adChatSendBtn}            id=com.m800.liveconnectdemo:id/lc_btn_send
${adChatCloseBtn}           id=com.m800.liveconnectdemo:id/live_connect_close
${adChatBackBtn}            xpath=/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.view.ViewGroup/android.widget.ImageButton
${adChatInputField}         id=com.m800.liveconnectdemo:id/lc_et_sendMessage

${adChatCloseTitle}         Close Inquiry
${adChatCloseKeep}          id=android:id/button2
${adChatCloseClose}         id=android:id/button1

${adChatClosedTitle}        Inquiry is Terminated
${adChatClosedFind}         FIND ANOTHER AGENT
${adChatClosedClose}        LEAVE CHATROOM
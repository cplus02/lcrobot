*** Settings ***
Library                     String
Library                     AppiumLibrary
Resource                    adDeviceCfg.robot
Resource                    adAppElement.robot

*** Keywords ***
Initialize Android Device
    [Arguments]  ${adDevice}=adDevice01
    AppiumLibrary.Open Application  ${appiumUrlAd}  automationName=${automationNameAd}  platformName=${platformNameAd}
    ...  appPackage=${appPackageAd}  appActivity=${appActivityAd}
    ...  platformVersion=&{${adDevice}}[platformVersion]  deviceName=&{${adDevice}}[deviceName]
    ...  udid=&{${adDevice}}[udid]  noReset=&{${adDevice}}[noReset]

## Directory
Make An Inquiry By Im On Android App Client
    AppiumLibrary.Click Element  ${adWidgetButton}
    Sleep  1s
    ${status}  Run Keyword And Return Status  AppiumLibrary.Page Should Contain Text  Finding agent
    Run Keyword If  ${status}  Run Keywords  AppiumLibrary.Click Element  ${adChatCloseBtn}
    ...  AND  AppiumLibrary.Wait Until Page Contains  ${adChatCloseTitle}  timeout=30s
    ...  AND  AppiumLibrary.Click Element  ${adChatCloseClose}
    ${status}  Run Keyword And Return Status  AppiumLibrary.Page Should Contain Text  ${adChatClosedTitle}
    Run Keyword If  ${status}  AppiumLibrary.Click Text  ${adChatClosedClose}
    AppiumLibrary.Wait Until Page Contains Element  ${adDirectoryPageSearchBtn}  timeout=30s
    AppiumLibrary.Click Element  ${adDirectoryPageSearchBtn}
    AppiumLibrary.Input Text  ${adDirectoryPageSearch}  TW Dev
    AppiumLibrary.Click Element  ${adDirectoryImButton}

## Chat
Send Message From App Client
    [Arguments]  ${messageString}
    AppiumLibrary.Wait Until Page Contains Element  ${adChatMicBtn}
    AppiumLibrary.Element Should Be Enabled  ${adChatMicBtn}
    AppiumLibrary.Input Text  ${adChatInputField}  ${messageString}
    AppiumLibrary.Wait Until Page Contains Element  ${adChatSendBtn}
    AppiumLibrary.Click Element  ${adChatSendBtn}
    AppiumLibrary.Page Should Contain Text  ${messageString}

Close App Client Chatroom
    ${status}  Run Keyword And Return Status  AppiumLibrary.Element Should Be Enabled  ${adChatCloseBtn}
    Run Keyword If  ${status}  AppiumLibrary.Click Element  ${adChatCloseBtn}
    AppiumLibrary.Wait Until Page Contains  ${adChatCloseTitle}  timeout=30s
    AppiumLibrary.Click Element  ${adChatCloseClose}
    Sleep  2s

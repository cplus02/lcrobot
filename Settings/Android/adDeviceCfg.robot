*** Settings ***
Documentation               Android devices configuration file, including udid, appium server URL...etc

*** Variables ***
## Common system environment variables for Android device
${appiumUrlAd}              http://localhost:4723/wd/hub
${automationNameAd}         UiAutomator2
${platformNameAd}           Android
${appPackageAd}             com.m800.liveconnectdemo  ##For testbed environment
${appActivityAd}            com.m800.liveconnect.hostapp.MainActivity

## Add real Android device info here
&{adDevice01}              platformVersion=8.1  deviceName=Mi A1  browserName=Chrome
...                        udid=d3a96af90504  noReset=true
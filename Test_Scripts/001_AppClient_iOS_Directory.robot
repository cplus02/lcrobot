*** Settings ***
Force Tags                  ios  appclient
Library                     AppiumLibrary
Library                     Selenium2Library
Resource                    ../Settings/TestRail/TestRailSetting.robot
Resource                    ../Settings/iOS/iOSKeywordLib.robot
Resource                    ../Settings/iOS/iOSAppElement.robot
Resource                    ../Settings/Web/webKeywordLib.robot
Resource                    ../Settings/Web/webElement.robot

*** Test Cases ***
Click Widget Button on Landing Page will direct to Directory listing Page
    [Documentation]  Expected result:
    ...              1. Directory page should be loaded correctly
    ...              2. Call/IM button on agent list should be enabled
    [Tags]  bat  directory  directorybutton
    [Setup]  Run Keywords  Initialize iOS Device  iOSDevice01  AND  Login To Web Agent  AND  Reset Web Agent Test Env
    AppiumLibrary.Click Element  ${iosWidgetButton}
    Sleep  2s  ## Backward compatible for slower devices
    AppiumLibrary.Page Should Contain Element  ${iosDirectoryPageTitle}
    AppiumLibrary.Input Text  ${iosDirectoryPageSearch}  TW Dev
    AppiumLibrary.Element Should Be Enabled  xpath=//*[@name="LiveConnect"]/XCUIElementTypeWindow[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]/XCUIElementTypeCell[1]/XCUIElementTypeButton[1]
    AppiumLibrary.Element Should Be Enabled  xpath=//*[@name="LiveConnect"]/XCUIElementTypeWindow[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]/XCUIElementTypeCell[1]/XCUIElementTypeButton[2]
    [Teardown]  Run Keywords  Update Test Case Status  400402  ${TEST STATUS}  ${TEST MESSAGE}  AND  AppiumLibrary.Close All Applications  AND  Selenium2Library.Close All Browsers
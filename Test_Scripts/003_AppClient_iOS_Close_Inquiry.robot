*** Settings ***
Force Tags                  ios  appclient
Library                     AppiumLibrary
Library                     Selenium2Library
Resource                    ../Settings/TestRail/TestRailSetting.robot
Resource                    ../Settings/iOS/iOSKeywordLib.robot
Resource                    ../Settings/iOS/iOSAppElement.robot
Resource                    ../Settings/Web/webKeywordLib.robot
Resource                    ../Settings/Web/webElement.robot

*** Test Cases ***
Client Close Inquiry
    [Tags]  bat
    [Setup]  Run Keywords  Initialize iOS Device  iOSDevice01  AND  Login To Web Agent  AND  Reset Web Agent Test Env
    Make An Inquiry By Im On iOS App Client
    Web Agent Pick Up Im Inquiry
    Close App Client Chatroom
    AppiumLibrary.Page Should Contain  ${iosDirectoryPageTitle}
    [Teardown]  Run Keywords  Update Test Case Status  403507  ${TEST STATUS}  ${TEST MESSAGE}  AND  AppiumLibrary.Close All Applications  AND  Selenium2Library.Close All Browsers
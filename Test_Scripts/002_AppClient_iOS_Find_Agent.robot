*** Settings ***
Force Tags                  ios  appclient
Library                     AppiumLibrary
Library                     Selenium2Library
Resource                    ../Settings/TestRail/TestRailSetting.robot
Resource                    ../Settings/iOS/iOSKeywordLib.robot
Resource                    ../Settings/iOS/iOSAppElement.robot
Resource                    ../Settings/Web/webKeywordLib.robot
Resource                    ../Settings/Web/webElement.robot

*** Test Cases ***
Client Can Successfully Make An Inquiry
    [Documentation]  Expected Result:
    ...              1. Agent can see the inquiry
    ...              2. Message icon, now Timestamp, client's name, tag name and language code can be seen for new inquiry at Inquiry list
    ...              3. Call button, file sharing and send button is disabled for App client
    ...              4. Close Inquiry option and backward button is enabled for App client
    [Tags]  bat  findagent  makeinquirybyim
    [Setup]  Run Keywords  Initialize iOS Device  iOSDevice01  AND  Login To Web Agent  AND  Reset Web Agent Test Env
    Make An Inquiry By Im On iOS App Client
    AppiumLibrary.Wait Until Page Contains Element  ${iosChatFindingAgent}  timeout=30s
    ## Verify web agent's page contains element needed
    Selenium2Library.Wait Until Page Contains Element  ${webAgentIncomingImName}  timeout=30s  ## Verify expected result 1
    Selenium2Library.Element Text Should Be  ${webAgentIncomingImName}  visitor
    Selenium2Library.Page Should Contain Element  ${webAgentIncomingImTag}
    Selenium2Library.Page Should Contain Element  ${webAgentIncomingImTime}
    Selenium2Library.Page Should Contain Element  ${webAgentIncomingImLng}
    Selenium2Library.Page Should Contain Element  ${webAgentIncomingImIcon}
    ## Verify app client's element status, verify expected result 2 and 3
    AppiumLibrary.Element Should Be Disabled  ${iosChatCallBtn}
    AppiumLibrary.Element Should Be Disabled  ${iosChatFileSharingBtn}
    AppiumLibrary.Element Should Be Disabled  ${iosChatSendBtn}
    ## AppiumLibrary.Element Should Be Enabled  ${iosChatCloseBtn}  ## iOS HostApp bug
    AppiumLibrary.Element Should Be Enabled  ${iosChatBackBtn}
    [Teardown]  Run Keywords  Update Test Case Status  400914  ${TEST STATUS}  ${TEST MESSAGE}  AND  AppiumLibrary.Close All Applications  AND  Selenium2Library.Close All Browsers

Agent pick up the inquiry then client can send message out
    [Documentation]  Expected Result:
    ...              1. Chatroom between web agent and app client has been created on both Dashboard and App Client
    ...              2. Call button, Close(X) Inquiry option, Backward button, File sharing and Send button is enabled
    ...              3. Web agent receive message from app client
    [Tags]  bat  findagent  makeinquirybyim
    [Setup]  Run Keywords  Initialize iOS Device  iOSDevice01  AND  Login To Web Agent  AND  Reset Web Agent Test Env
    Make An Inquiry By Im On iOS App Client
    Web Agent Pick Up Im Inquiry  ## This keyword contains verification of chatroom created
    
    ## Verify expected result 2
    AppiumLibrary.Element Should Be Disabled  ${iosChatCallBtn}
    AppiumLibrary.Element Should Be Enabled  ${iosChatFileSharingBtn}
    AppiumLibrary.Element Should Be Enabled  ${iosChatSendBtn}
    AppiumLibrary.Element Should Be Enabled  ${iosChatCloseBtn}
    AppiumLibrary.Element Should Be Enabled  ${iosChatBackBtn}

    ## Verify expected result 3
    ${messageString}  Generate Random String  10  [LETTERS][NUMBERS]  ## Generate a random string
    Send Message From App Client  ${messageString}
    Sleep  2s
    Selenium2Library.Page Should Contain  ${messageString}
    [Teardown]  Run Keywords  Update Test Case Status  402983  ${TEST STATUS}  ${TEST MESSAGE}  AND  AppiumLibrary.Close All Applications  AND  Selenium2Library.Close All Browsers
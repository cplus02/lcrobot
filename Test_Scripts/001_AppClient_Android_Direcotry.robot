*** Settings ***
Force Tags                  android  appclient
Library                     AppiumLibrary
Library                     Selenium2Library
Resource                    ../Settings/TestRail/TestRailSetting.robot
Resource                    ../Settings/Android/adKeywordLib.robot
Resource                    ../Settings/Android/adAppElement.robot
Resource                    ../Settings/Web/webKeywordLib.robot
Resource                    ../Settings/Web/webElement.robot

*** Test Cases ***
Click Widget Button on Landing Page will direct to Directory listing Page
    [Documentation]  Expected result:
    ...              1. Directory page should be loaded correctly
    ...              2. Call/IM button on agent list should be enabled
    [Tags]  bat  directory  directorybutton
    [Setup]  Run Keywords  Initialize Android Device  adDevice01  AND  Login To Web Agent  AND  Reset Web Agent Test Env
    AppiumLibrary.Click Element  ${adWidgetButton}
    AppiumLibrary.Wait Until Page Contains Element  ${adDirectoryPageSearchBtn}
    AppiumLibrary.Page Should Contain Text  ${adDirectoryPageTitle}
    AppiumLibrary.Click Element  ${adDirectoryPageSearchBtn}
    AppiumLibrary.Input Text  ${adDirectoryPageSearch}  TW Dev
    AppiumLibrary.Element Should Be Enabled  ${adDirectoryCallButton}
    AppiumLibrary.Element Should Be Enabled  ${adDirectoryImButton}
    [Teardown]  Run Keywords  Update Test Case Status  400402  ${TEST STATUS}  ${TEST MESSAGE}  AND  AppiumLibrary.Close All Applications  AND  Selenium2Library.Close All Browsers
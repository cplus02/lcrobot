*** Settings ***
Force Tags                  android  appclient
Library                     AppiumLibrary
Library                     Selenium2Library
Resource                    ../Settings/TestRail/TestRailSetting.robot
Resource                    ../Settings/Android/adKeywordLib.robot
Resource                    ../Settings/Android/adAppElement.robot
Resource                    ../Settings/Web/webKeywordLib.robot
Resource                    ../Settings/Web/webElement.robot

*** Test Cases ***
Client Close Inquiry
    [Tags]  bat
    [Setup]  Run Keywords  Initialize Android Device  adDevice01  AND  Login To Web Agent  AND  Reset Web Agent Test Env
    Make An Inquiry By Im On Android App Client
    Web Agent Pick Up Im Inquiry
    Close App Client Chatroom
    AppiumLibrary.Page Should Contain Text  ${adDirectoryPageTitle}
    [Teardown]  Run Keywords  Update Test Case Status  403507  ${TEST STATUS}  ${TEST MESSAGE}  AND  AppiumLibrary.Close All Applications  AND  Selenium2Library.Close All Browsers